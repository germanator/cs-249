package p1_package;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class EncryptionClass 
   {
    /**
     * 
     */
    private int arrSide;
    
     /**
     * 
     */
    private int[][] encryptedArr;
    
     /**
     * Class Global FileReader variable so methods can be used
     */
    private FileReader fileIn;
    
    /**
     * Constant for maximum input char limit
     */
    private final int MAX_INPUT_CHARS = 256;
    
    /**
     * Constant for minus sign used in getAnInt
     */
    private final char MINUS_SIGN = '-';
    
    /**
     * Constant for unprintable char value used as message end char
     */
    private final int UNPRINTABLE_CHAR_VALUE = 127; // ASCII for delete key
    
    /**
     * 
     */
    public EncryptionClass()
      {
        
      }
    
    /**
     * 
     * @param copied 
     */
    public EncryptionClass(EncryptionClass copied)
       {
        
        
           
       }
    
    
    
    public boolean charInString( char testChar, String testString )
       {
        int charCounter = 0;
        for( ; charCounter < testString.length(); charCounter++ )
        {
         if( testString.charAt(charCounter) == testChar )
           {
            return true;
           }
        }
        return false;
       }
    
    
    public String decryptData()
       {
        return "sdasdsa";
       }
    
    public void displayCharArray()
       {
        int rowCounter = 0;
        int colCounter = 0;
        System.out.println("Diagnostic Array Display");
        for(; rowCounter < encryptedArr.length; rowCounter++)
           {
               for(; colCounter < encryptedArr[rowCounter].length; colCounter++)
                  {
                   System.out.println("\t" + encryptedArr[rowCounter][colCounter]);
                  }
           }
       }
      
    /**
     * Downloads encrypted data to file
     * <p>
     * Note: No action taken if array is empty
     * 
     * @param fileName String object holding file name to use
     */
    void downloadData( String fileName )
       {
        FileWriter toFile;
       
        int rowIndex, colIndex;
       
        if( arrSide > 0 )
           {
            try
               {
                toFile = new FileWriter( fileName );
            
                toFile.write( "" + arrSide + "\r\n" );
           
                for( rowIndex = 0; rowIndex < arrSide; rowIndex++ )
                   {
                    for( colIndex = 0; colIndex < arrSide; colIndex++ )
                       {
                        if( encryptedArr[ rowIndex ][ colIndex ] < 100 )
                           {
                            toFile.write( "0" );
                           }
                        
                        toFile.write("" 
                                 + encryptedArr[ rowIndex ][ colIndex ] + " " );
                       }
                 
                    toFile.write( "\r\n" );
                   }
           
                toFile.flush();
                toFile.close();
               }
       
            catch( IOException ioe )
               {
                ioe.printStackTrace();
               }
           }
       }
    public void encryptData(String toEncrypt)
       {
        
       }
    
    private double findSquareRoot(int value)
       {
        int min = 0;
        int max = value / 2;
        int guess = (max - min) / 2;
        while(Math.abs(guess * guess - value) > 0.000001)
           {
            if(guess * guess > value)
            {
                max = guess;
            }
            else
            {
                min = guess;
            }
            guess = (max - min) / 2;
           }
           return guess;
       }

    /**
     * gets an integer from the input stream
     * 
     * @param maxLength maximum length of characters
     * input to capture the integer
     * 
     * @return integer captured from file
     */
    private int getAnInt( int maxLength )
       {
        int inCharInt;
        int index = 0;
        String strBuffer = "";
        int intValue;
        boolean negativeFlag = false;

        try
           {
            inCharInt = fileIn.read();

            // clear space up to number
            while( index < maxLength && !charInString( (char)inCharInt, 
                                                           "0123456789+-" ) )
               {
                inCharInt = fileIn.read();
               
                index++;
               }
           
            if( inCharInt == MINUS_SIGN )
               {
                negativeFlag = true;
               
                inCharInt = fileIn.read();
               }

            while( charInString( (char)inCharInt, "0123456789" ) )
               {   
                strBuffer += (char)( inCharInt );

                index++;
               
                inCharInt = fileIn.read();
               }            
           }
       
        catch( IOException ioe )
           {
            System.out.println( "INPUT ERROR: Failure to capture character" );
          
            strBuffer = "";
           }
          
        intValue = Integer.parseInt( strBuffer );
       
        if( negativeFlag )
           {
            intValue *= -1;
           }
       
        return intValue;
       }

    private int getRandomCharValue()
    {
        return (int)(Math.random() * (double)UNPRINTABLE_CHAR_VALUE);
    }
    
    
    
    /**
     * Uploads data from file holding a square array
     * 
     * @param fileName String object holding file name
     */
    void uploadData( String fileName )
       { 
        int rowIndex, colIndex;
      
        try
           {
            // Open FileReader 
            fileIn = new FileReader( fileName );
        
            // get side length
            arrSide = getAnInt( MAX_INPUT_CHARS );           
      
            encryptedArr = new int[ arrSide ][ arrSide ];
            
            for( rowIndex = 0; rowIndex < arrSide; rowIndex++ )
               {
                for( colIndex = 0; colIndex < arrSide; colIndex++ )
                   {
                    encryptedArr[ rowIndex ][ colIndex ] 
                                                  = getAnInt( MAX_INPUT_CHARS );
                   }
               }
            
            fileIn.close();
           }
      
        // for opening file
        catch( FileNotFoundException fnfe )
           {
            fnfe.printStackTrace();
           }
        
        // for closing file
        catch (IOException ioe)
           {
            ioe.printStackTrace();
           }
        
       }  
         
   }    
    
    