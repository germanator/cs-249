package p10_package;

/**
 *
 * @author Chris Schild
 */
public class SimpleBSTClass {
    
    /*
    * Root of BST
    */
    private SimpleStudentClass treeRoot;
    
    /*
    * Default class constructor
    */
    public SimpleBSTClass()
        {
         treeRoot = null;
        }
    
    /*
    * Copy constructor
    */
    public SimpleBSTClass( SimpleBSTClass copied )
        {
         treeRoot = copyConstructorHelper( copied.treeRoot );
        }
    
    /*
    * Clears Tree
    */
    public void clearTree()
        {
         treeRoot = null;
        }
    
    /*
    * Recursive copy constructor helper
    * @param wkgCopiedRef - Node reference to SimpleStudentClass node
    * @return SimpleStudentClass reference to link to calling method/node
    */
    private SimpleStudentClass copyConstructorHelper( SimpleStudentClass
                                                                  wkgCopiedRef )
        {
         if( wkgCopiedRef != null )
            {
             treeRoot = new SimpleStudentClass( wkgCopiedRef );
             treeRoot.leftChildRef = copyConstructorHelper( 
                                                    wkgCopiedRef.leftChildRef );
             treeRoot.rightChildRef = copyConstructorHelper(
                                                   wkgCopiedRef.rightChildRef );
            }
         return wkgCopiedRef;
        }
    
    /*
    * Displays BST in order
    */
    public void displayInOrder()
        {
         displayInOrderHelper( treeRoot );
        }
    
    /*
    * Provides inOrder traversal action using recursion
    * @param wkgRef - Node tree root reference at the current recursion level
    */
    private void displayInOrderHelper( SimpleStudentClass wkgRef )
        {
         if( wkgRef != null )
            {
             displayInOrderHelper( wkgRef.leftChildRef );
             System.out.println( wkgRef.toString() );
             displayInOrderHelper( wkgRef.rightChildRef );
            }
        }
    
    /*
    * Returns larger of two values
    * @param one - one of two values to be tested
    * @param other - other of two values to be tested
    * @return highest value of the two input values
    */
    private int getMax( int one, int other )
        {
         if ( one > other )
            {
             return one; 
            }
         return other;
        }
    
     /*
    * Finds height of BST Note: empty tree: -1;
    * root node only: 0; number of edges thereafter
    * @param wkgLocalRef - Node node from which height is found
    * @return integer height of tree
    */
    public int getTreeHeight()
        {
         if( isEmpty() )
            {
            return -1; 
            }
         return treeHeightHelper( treeRoot );
        }
    
    /*
    * Insert method for BST
    * <p>
    * Note: Overloaded insert uses insert method with individual
    * student information data items
    * @param inName - name data to be added to BST
    * @param inStudentID - student ID data to be added to BST
    * @param inGender - gender data to be added to BST
    * @param inGPA - gpa data to be added to BST
    * @return Boolean result of action
    */
    public boolean insert( String inName, int inStudentID, char inGender,
                                                                  double inGPA )
        {
          SimpleStudentClass newStudent = new SimpleStudentClass( inName,
                                                 inStudentID, inGender, inGPA );
         return insertHelper( treeRoot, newStudent );
        }
    
     /*
    * Insert method for BST
    * <p>
    * Note: Overloaded insert uses insert helper method with
    * a SimpleStudentClass object
    * @param newNode - SimpleStudentClass object to be added to BST
    * @return Boolean result of action
    */
    public boolean insert( SimpleStudentClass newNode )
        {
         return insertHelper( treeRoot, newNode );
        }
    
    /*
    * Insert helper method for BST insert action
    * <p>
    * Note: Does not allow duplicate entries (i.e., duplicate student IDs)
    * @param wkgRef - SimpleStudentClass tree root 
    * reference at the current recursion level
    * @param newNode - SimpleStudentClass object to be added to BST
    * @return Boolean result of insertion action
    */
    private boolean insertHelper( SimpleStudentClass wkgRef,
                             SimpleStudentClass newNode )
    {
     if( isEmpty() )
        {
         treeRoot = new SimpleStudentClass( newNode );
         return true;
        }
     if( wkgRef.studentID < newNode.studentID )
        {
         if( wkgRef.rightChildRef != null )
            {
             return insertHelper( wkgRef.rightChildRef, newNode );
            }
        else
            {
             wkgRef.rightChildRef = new SimpleStudentClass( newNode );
             return true;
            }
        }
     else if( wkgRef.studentID > newNode.studentID )
        {
         if( wkgRef.leftChildRef != null )
            {
             return insertHelper( wkgRef.leftChildRef, newNode );
            }
        else
            {
             wkgRef.leftChildRef = new SimpleStudentClass( newNode );
             return true;
            }
        }
     else
        {
         return false; 
        }
    }
    
    /*
    * Test for empty tree
    * @return Boolean result of test
    */
    public boolean isEmpty()
        {
         return treeRoot == null;
        }
    
    /*
    * Recursively searches tree from given node to minimum value node,
    * stores data value found, and then unlinks the node
    * @param minParent - SimpleStudentClass reference to parent of child node;
    * used for linking from parent to child's right child
    * @param minChild - SimpleStudentClass reference to child node to be tested
    * @return SimpleStudentClass reference containing removed node
    */
    
    private SimpleStudentClass removeFromMin( SimpleStudentClass 
                                  minParent, SimpleStudentClass minChild )
        {
         if( minChild.leftChildRef != null )
            {
             return removeFromMin( minChild, minChild.leftChildRef );
            }
         minParent.leftChildRef = minChild.rightChildRef;
         return minChild;
        }
    
    /*
    * Removes data node from tree using given key
    * <p>
    * Note: verifies data node is in the tree, then uses remove helper method
    * @param studentID - item that is used for search/removal
    * @return SimpleStudenClass result of remove action
    */
    public SimpleStudentClass removeNode( int studentID )
        {
         if( search( studentID ) != null )
            {
             
             return removeNodeHelper(treeRoot, studentID); 
            }
         return null;
        }
    
    /*
    * Recursive helper for BST remove action
    * <p>
    * Note: uses removeFromMin method
    * <p>
    * Note: Since removeNode calling method verifies data node existence, 
    * this method does not need to treat missing node condition
    * @param wkgRef - SimpleStudentClass tree 
    * root reference at the current recursion level
    * @param studentID - item that is used for search/removal
    * @return SimpleStudentClass reference result of remove helper action
    */
    private SimpleStudentClass removeNodeHelper( SimpleStudentClass wkgRef,
                                                                 int studentID )
        {
            if( wkgRef.studentID == studentID )
                {
                 SimpleStudentClass oldRoot = wkgRef;
                 if( wkgRef.leftChildRef == null )
                    {
                      wkgRef = wkgRef.rightChildRef;
                    }
                 else if( wkgRef.rightChildRef == null )
                    {
                     wkgRef = wkgRef.leftChildRef; 
                    }
                 else
                    {
                     wkgRef = removeFromMin( wkgRef.rightChildRef,
                                          wkgRef.rightChildRef.leftChildRef );
                     wkgRef.leftChildRef = oldRoot.leftChildRef;
                     wkgRef.rightChildRef = oldRoot.rightChildRef;
                    }
                 return oldRoot;
                }
            else if( studentID > wkgRef.studentID )
                {
                 return removeNodeHelper( wkgRef.rightChildRef, studentID );
                }
            else
                {
                return removeNodeHelper( wkgRef.leftChildRef, studentID );
                }
        }
    
    /*
    * Searches for data in BST given necessary student ID key
    * @param studentID - search value
    * @return SimpleStudentClass reference to found data
    */
    public SimpleStudentClass search( int studentID )
        {
         return searchHelper( treeRoot, studentID );
        }
    
    /*
    * Helper method for BST search action
    * @param wkgRef - SimpleStudentClass working reference
    * at the current recursion level
    * @param studentID - item containing key (student ID number)
    * @return Boolean result of search
    */
    private SimpleStudentClass searchHelper( SimpleStudentClass wkgRef,
                                        int studentID )
        {
         if( isEmpty() || wkgRef == null )
            {
              return null;
            }
         if( wkgRef.studentID == studentID )
            {
              return wkgRef;
            }
         if( wkgRef.studentID > studentID )
            {
             return searchHelper( wkgRef.leftChildRef, studentID );
            }
         else
            {
             return searchHelper( wkgRef.rightChildRef, studentID ); 
            }
        }
    
    /*
    * Helper method to find height of BST
    * @param wkgRef - SimpleStudentClass working reference
    * used at the current level of recursion
    * @return height of tree - maximum number of edges 
    * from root node to lowest part of tree
    */
    private int treeHeightHelper( SimpleStudentClass wkgRef )
        {
         if( wkgRef == null )
            {
             return -1;
            }
         else if( treeRoot != null && treeRoot.leftChildRef == null &&
                                                treeRoot.rightChildRef == null )
            {
             return 0;
            }
         else
            {
             int leftDepth = treeHeightHelper( wkgRef.leftChildRef ); 
             int rightDepth = treeHeightHelper( wkgRef.rightChildRef );
             if( leftDepth > rightDepth )
                {
                 return leftDepth + 1;
                }
             return rightDepth + 1;
            }
        } 
    }