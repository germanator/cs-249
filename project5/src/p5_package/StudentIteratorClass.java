package p5_package;

public class StudentIteratorClass {
    /*
    * Current index of iterator
    */
    private int currentIndex;
    
    /*
    * StudentClassList data used by this class
    */
    private StudentListClass iteratorList;
    
    /*
    * Constant character for display
    */
    private final char LEFT_BRACKET = 91;
    /*
    * Constant character for display
    */
    private final char RIGHT_BRACKET = 93;
    
    /*
    * Constant character for display
    */
    private final char SPACE = 32;
    
    /*
    * Default constructor for StudentIteratorClass
    */
    public StudentIteratorClass( )
        {
         currentIndex = 1;
         iteratorList = new StudentListClass( );
        }
    
    /*
    * Initialization constructor for StudentIteratorClass
    */
    public StudentIteratorClass( int initCapacity )
        {
         currentIndex = 1;
         iteratorList = new StudentListClass( initCapacity );
        }
    
    /*
    * Copy constructor for StudentIteratorClass
    */
    public StudentIteratorClass( StudentIteratorClass copied )
        {
         currentIndex = 1;
         iteratorList = new StudentListClass( copied.iteratorList );
        }
    
    /*
    * Clears array
    */
    public void clear( )
        {
         setToBeginning( );
         iteratorList = new StudentListClass( );
        }
    
    /*
    * Recursive method displays spaces for runDiagnosticDisplay
    * @param numSpaces - integer value specifying number of spaces to display
    */
    private void displaySpaces( int numSpaces )
        {
         if ( numSpaces > 0 )
            {
             displaySpaces( numSpaces - 1 );
            }
         System.out.print( SPACE );
        }
    
    /*
    * Gets value at iterator cursor location
    * @returns StudentClass object returned; null if not found
    */
    public StudentClass getAtCurrent( )
        {
         return iteratorList.getNthStudent( currentIndex );
        }
    
    /*
    * Inserts new value after value at iterator cursor
    * <p>
    * Note: Current value must remain the same after data set,
    * unless a value is added to the empty list
    * @param newValue - StudentClass object to be inserted in list
    * @return Boolean result of action; true if successful, false otherwise
    */
    public boolean insertAfterCurrent( StudentClass newValue )
        {
         Boolean success = iteratorList.
                            insertDataAtNthPosition( currentIndex + 1, newValue );
         if( success )
            {
             return true;
            }
         return  false;
        }
    
    /*
    * Inserts new StudentClass object before item at iterator cursor
    * <p>
    * Note: Current value must remain the same after data set, unless a value is added to the empty list
    * @param newValue - StudentClass object to be inserted in list
    * @return Boolean result of action; true if successful, false otherwise
    */
    public boolean insertBeforeCurrent( StudentClass newValue )
        {
         Boolean success = iteratorList.
                            insertDataAtNthPosition( currentIndex, newValue );
         if( success )
            {
             moveNext( );
             return true;
            }
         return false;
        }
    
    /*
    * Reports if iterator cursor is at beginning
    * If list is empty, cursor is not at beginning
    * @return Boolean result of action; true if at beginning, false otherwise
    */
    public boolean isAtBeginning( )
        {   
         if ( isEmpty( ) || currentIndex == 1 )
            {
             return false;
            }
         return true;
        }
    
    /*
    * Reports if iterator cursor is at end
    * If list is empty, cursor is not at end
    * @return Boolean result of action; true if at end, false otherwise
    */
    public boolean isAtEnd( )
        {
        if ( isEmpty( ) || currentIndex == iteratorList.getCurrentSize( ) - 1 )
            {
             return false;
            }
         return true;
        }
    
    /*
    * Reports if list is empty
    * @return Boolean result of action; true if empty, false otherwise
    */
    public boolean isEmpty( )
        {
        return iteratorList.isEmpty( );
        }
    
    /*
    * If possible, moves iterator cursor one position to the right, or next
    * @return Boolean result of action; true if successful, false otherwise
    */
    public boolean moveNext( )
        {
         if( isAtEnd( ) )
            {
             return false;
            }
         currentIndex += 1;
         return true;
        }
    
    /*
    * If possible, moves iterator cursor one position to the left, or previous
    * @returns Boolean result of action; true if successful, false otherwise
    */
    public boolean movePrev( )
        {
         if( isAtBeginning( ) )
            {
             return false;
            }
         currentIndex -= 1;
         return true;
        }
    
    /*
    * Removes and returns a data value from the iterator cursor position
    * <p>
    * Note: cursor must be located at succeeding element unless last item removed 
    * @return StudentClass object removed from list, or null if not found
    */
    public StudentClass removeAtCurrent( )
        {
         return iteratorList.removeNthStudent( currentIndex );
        }
    
    /*
    * Replaces item at iterator cursor with new StudentClass object
    * @param newValue - StudentClass object to be replaced in list
    * @return Boolean result of action; true if successful, false otherwise
    */
    public boolean replaceAtCurrent( StudentClass newValue )
        {
         return iteratorList.insertDataAtNthPosition( currentIndex, newValue );
        }
    
    /*
    * Shows space-delimited list with cursor location indicated
    */
    public void runDiagnosticDisplay( )
        {
         int iteratorCtr = 1;
         System.out.println( "Left End of Iterator:" );
         for( ; iteratorCtr <= iteratorList.getCurrentSize( ); iteratorCtr++ )
            {
             displaySpaces( iteratorCtr );
             if( currentIndex == iteratorCtr )
                {
                 System.out.print( LEFT_BRACKET );
                 System.out.print( iteratorList.getNthStudent( iteratorCtr ).name );
                 System.out.print( RIGHT_BRACKET );
                 System.out.println( "" );
                }
             else 
                {
                 System.out.println( iteratorList.getNthStudent( iteratorCtr ).name );
                }
            }
         displaySpaces( iteratorCtr );
         System.out.println( "Right End of Iterator" );
        }
    
    /*
    * Sets iterator cursor to beginning of list
    * @return Boolean result of action; true if successful, false otherwise
    */
    public boolean setToBeginning( )
        {
         currentIndex = 0;
         return true;
        }
    
    /*
    * Sets iterator cursor to the end of the list
    * @return Boolean result of action; true if successful, false otherwise
    */
    public boolean setToEnd( )
        {
        currentIndex = iteratorList.getCurrentSize( );
        return true;
        }   
    }
