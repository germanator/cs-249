package p5_package;


public class StudentListClass {
    
    /*
    * Default constant capacity
    */
    private static final int DEFAULT_CAPACITY = 10;
    
    /*
    * For N not found in search
    */
    private static final int NOT_FOUND = -1;
    
    /*
    * Member data
    */
    private int arrayCapacity;
    
    /*
    * Member data
    */
    private int arraySize;
    
    /*
    * Member - StudentClass array
    */
    private StudentClass[] studentArray;
    
    /*
    * Default constructor, initializes array to default capacity
    */
    public StudentListClass( )
        {
         arraySize = 0;
         arrayCapacity = DEFAULT_CAPACITY;
         int initCtr = 0;
         studentArray = new StudentClass[ DEFAULT_CAPACITY ];
         for( ; initCtr < DEFAULT_CAPACITY; initCtr++ )
            {
             studentArray[ initCtr ] = null;
            }
        }
    
    /*
    * Initializing constructor, initializes array to specified capacity
    * <p>
    * Note: Does not allow capacity less than default capacity
    * @param capacity - integer initial capacity specification for the array
    */
    public StudentListClass( int capacity )
        {
         arraySize = 0;
         int initCtr = 0;
         if ( capacity < DEFAULT_CAPACITY )
            {
             arrayCapacity = DEFAULT_CAPACITY;
            }
         else {
             arrayCapacity = capacity;
         }
         studentArray = new StudentClass[ capacity ];
         for( ; initCtr < capacity; initCtr++ )
            {
             studentArray[ initCtr ] = null;
            }
        }
    
    /*
    * Copy constructor, initializes array to size of copied array,
    * and capacity of copied array, then copies only the elements
    * up to the given size
    * @param copied - StudentListClass object to be copied
    */
    public StudentListClass( StudentListClass copied )
        {
         arrayCapacity = copied.arrayCapacity;
         arraySize = copied.arraySize;
         int initCtr = 0;
         studentArray = new StudentClass[ arraySize ];
         for( ; initCtr < arraySize; initCtr++ )
            {
             studentArray[ initCtr ] = null;
            }
        }
    
    /*
    * Sets data at end of list
    * <p>
    * Note: Uses insertDataAtNthPosition
    * @param student - StudentClass object to be appended to list
    */
    public void appendDataAtEnd( StudentClass student )
        {
         //checkForResize( );
         insertDataAtNthPosition( arraySize + 1, student );
        }
    
    /*
    * Description: Checks for need to resize; if this is necessary,
    * creates new array with double the original capacity,
    * loads data from original array to new one,
    * then sets studentArray to new array
    */
    private void checkForResize( )
        {
         if ( arraySize >= arrayCapacity )
            {
             arrayCapacity *= 2;
             StudentClass[ ] tempArray = new StudentClass[ arrayCapacity ];
             int copyCtr = 0;
             for( ; copyCtr < arraySize; copyCtr++ )
                {
                 tempArray[ copyCtr ] = studentArray[ copyCtr ]; 
                }
             studentArray = tempArray;
            }
        }
    
    /*
    * Clears array of all valid values by setting array size to zero,
    * values remain in array but are not accessible
    */
    public void clear(  )
        {
         arraySize = 0;
        }
    
    /*
    * Displays student list
    */
    public void displayList()
        {
         System.out.println( "Student Class List:" );
         int loopCtr = 1;
         for( ; loopCtr <= arraySize; loopCtr++ )
            {
             System.out.println( getNthStudent( loopCtr ) );
            }
         System.out.println( "" );
        }
    
    /*
    * Gets number of student if found in list
    * @param  student - StudentClass object for finding N
    * @return integer N of the StudentClass object, or NOT_FOUND if not in list
    */
    public int findStudentNumber( StudentClass student )
        {
         int findCtr = 0;
         while( findCtr < arraySize )
            {
             if ( student.compareTo( studentArray [ findCtr ] ) == 0 )
                {
                 return findCtr + 1;
                }
             findCtr++;
            }
         return NOT_FOUND;
        }
    
    /*
    * Description: Gets current size of array
    * <p>
    * Note: size of array indicates number of valid or viable values in the array
    * @return integer size of array
    */
    public int getCurrentSize( )
        {
         int largestValidEntry = 0;
         for( ; largestValidEntry < arraySize; largestValidEntry++)
            {
             if ( studentArray[ largestValidEntry ] == null )
                {
                 return largestValidEntry;
                }
            }
         return arraySize;
        }
    
    /*
    * Acquires the Nth item in the list, starting with N = 1
    * @param N_value - integer value to identify Nth student to retrieve
    * @return StudentClass value at element or null \
    * if attempt to acquire data out of bounds
    */
    public StudentClass getNthStudent( int N_value )
        {
         if ( N_value > arraySize || N_value < 1 )
            {
             return null;
            }
         return studentArray[ N_value - 1 ];
        }
    
    /*
    * Sets data at beginning of list; moves all subsequent data up by one element
    * <p>
    * Note: No failure mode; data will be set at beginning no matter what the size of the array is
    * @param student - StudentClass object to set at beginning
    */
    public void insertDataAtBeginning( StudentClass student )
        {
         checkForResize();
         arraySize++;
         int moveCtr = arraySize;
         for( ; moveCtr > 0; moveCtr--)
            {
             studentArray[ moveCtr ] = studentArray[ moveCtr - 1 ];
            }
             studentArray[ 0 ] = student;
        }
    
    /*
    * Description: Moves data up one element,
    * then sets item in array at specified Nth position, where N starts at 1
    * <p>
    * Note: Allows item to be appended to end of list
    * @param NthPos - integer value to indicate which position (N) \
    * at which to insert student data
    * @param student - StudentClass object to be inserted at Nth position \
    * (N - 1 in the array)
    * @return Boolean success if inserted, or failure if incorrect N was used
    */
    public boolean insertDataAtNthPosition(int NthPos,
                                       StudentClass student)
        {
         checkForResize();
         if( arraySize == 0 )
            {
             arraySize++;
             studentArray[ 0 ] = student;
             return true;
            }

         if( NthPos == arraySize )
            {
             arraySize++;
             studentArray[ NthPos ] = student;
             return true;
            }

         arraySize++;
         int moveCtr = arraySize;
         for( ; moveCtr > NthPos - 1; moveCtr-- )
            {
              studentArray[ moveCtr ] = studentArray[ moveCtr - 1 ];
            }
         
         studentArray[ NthPos - 1 ] = student;
         return true;
        }
    
    /*
    * Tests for size of array equal to zero, no valid values stored in array
    * @return Boolean result of test for empty
    */
    public boolean isEmpty( )
        {
        if( arraySize == 0 )
            {
             return true;
            }
        else
            {
             if( studentArray[0] == null )
                {
                 return true;
                }
             else
                {
                 return false;
                }
            }
        }
    
    /*
    * Description: Removes Nth item from array if index within array size bounds
    * <p>
    * Note: Each data item from the element immediately above the remove index \
    * to the end of the array is moved down by one element
    * @param numberN - integer number of element value to be removed, starts at N = 1
    * @return removed StudentClass value if successful, null if not
    */
    public StudentClass removeNthStudent( int numberN )
        {
            StudentClass tempStudent;
            if ( numberN > arraySize || numberN == 0 )
                {
                 return null;
                }
            if( numberN == 1 )
                {
                 tempStudent = studentArray[ numberN ];
                 studentArray[0] = null;
                 arraySize--;
                 return tempStudent;
                }
            else
                {
                 int loopCtr = numberN - 1;
                 tempStudent = studentArray[ numberN - 1 ];
                 for( ; loopCtr < arraySize; loopCtr++)
                    {
                     studentArray[ loopCtr ] = studentArray[ loopCtr + 1 ];   
                    }
                 arraySize--;
                }
         return tempStudent;
        }
    
    /*
    * Description: Replaces item in array at specified Nth position, where N starts at 1
    * @param NthPos - integer value to indicate Nth position at which to replace student data
    * @param student - StudentClass object to be inserted at Nth position (N - 1 in the array)
    * @return Boolean success if inserted, or failure if incorrect N was used
    */
    public boolean replaceDataAtNthPosition(int NthPos,
                                        StudentClass student)
        {
         if ( NthPos > arraySize )
            {
             return false;
            }
         studentArray[ NthPos - 1 ] = student;
         return true;
        }
    }
