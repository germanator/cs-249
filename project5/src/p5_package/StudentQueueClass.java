package p5_package;

public class StudentQueueClass extends StudentListClass {

    /*
    * Constant for displaying spaces
    */
    private static final String SPACE = " ";
    
    
    /*
    * Default constructor
    */
    public StudentQueueClass()
        {
         super();
        }
    
    /*
    * Initialization constructor
    */
    public StudentQueueClass(int capacity)
        {
         super(capacity);
        }
    
    /*
    * copy constructor
    */
    public StudentQueueClass(StudentQueueClass copied)
        {
         super(copied);
        }
    
    /*
    * Clears queue using parent's operation
    */
    public void clearQueue()
        {
         super.clear();
        }
    
    /*
    * Dequeues from end of list, front of queue
    * @return StudentClass object if available; null otherwise
    */
    public StudentClass dequeue()
        {
         StudentClass tempStudent = super.getNthStudent( super.getCurrentSize() );
         super.removeNthStudent( super.getCurrentSize() );
         return tempStudent;
        }
    
    /*
    * Displays queue from tail to head
    */
    public void displayQueue()
        {
         int iteratorCtr = 1;
         System.out.println( "Tail of Queue:" );
         for( ; iteratorCtr <= super.getCurrentSize(); iteratorCtr++ )
            {
             displaySpaces(iteratorCtr);
             System.out.println( super.getNthStudent( iteratorCtr ) );
            }
         displaySpaces(iteratorCtr + 1);
         System.out.println("Head of Queue");
        }
    
    /*
    * Recursive method displays spaces for displayQueue
    * @param numSpaces - integer value specifying number of spaces to display
    */
    private void displaySpaces(int numSpaces)
        {
         if ( numSpaces > 0 )
            {
             displaySpaces(numSpaces - 1);
            }
         System.out.print(SPACE);
        }
    
    /*
    * Enqueues to beginning of list, tail of queue
    * @param student - StudentClass object to be enqueued
    */
    public void enqueue(StudentClass student)
        {
         super.insertDataAtBeginning(student);
        }
    
    /*
    * Reports queue empty using parent's operation
    * @return Boolean result of empty test
    */
    @Override
    public boolean isEmpty()
        {
         return super.isEmpty();
        }
    
    /*
    * Peek at front of queue, no state change
    * return StudentClass object if available; null otherwise
    */
    public StudentClass peekFront()
        {
         return super.getNthStudent(0);
        }    

}