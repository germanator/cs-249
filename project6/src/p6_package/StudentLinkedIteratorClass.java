/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p6_package;

/**
 *
 * @author Chris
 */
public class StudentLinkedIteratorClass {
    
    /*
    * Constant character for display
    */
    private final char SPACE = ' ';
    
    /*
    * Constant character for display
    */
    private final char LEFT_BRACKET = '[';
    
    /*
    * Constant character for display
    */
    private final char RIGHT_BRACKET = ']';
    
    /*
    * StudentClassList data used by this class
    */
    private StudentLinkedListClass iteratorList;
    
    /*
    * Current index of iterator
    */
    private int currentIndex;
    
    /*
    * Default constructor for StudentIteratorClass
    */
    public StudentLinkedIteratorClass()
        {
         this.currentIndex = 1;
         this.iteratorList = new StudentLinkedListClass();
        }
    
    /*
    * Copy constructor for StudentIteratorClass
    * @param copied - StudentIteratorClass object to be copied
    */
    public StudentLinkedIteratorClass( StudentLinkedIteratorClass copied )
        {
         this.iteratorList = copied.iteratorList;
         this.currentIndex = copied.currentIndex;
        }
    
    /*
    * Clears array
    */
    public void clear()
        {
         this.iteratorList.clear();
         this.currentIndex = 1;
        }
    
    /*
    * Recursive method displays spaces for runDiagnosticDisplay
    * @param numSpaces - integer value specifying number of spaces to display
    */
    private void displaySpaces( int numSpaces )
        {
         if( numSpaces > 0 )
            {
             displaySpaces( numSpaces - 1 );        
            }
         System.out.print( SPACE );
        }
    
    /*
    * Gets value at iterator cursor location
    * @return StudentClass object returned; FAILED_ACCESS if not found
    */
    public StudentClass getAtCurrent()
        {
         StudentClass foundStudent = iteratorList.getNthStudent(currentIndex);
         if( foundStudent == null )
            {
             return null;
            }
         else
            {
             return foundStudent;
            }
        }
    
    /*
    * Inserts new value after value at iterator cursor
    * <p>
    * Note: Current value must remain the same after data set,
    * unless the list was empty prior to data set
    * @param newValue - StudentClass object to be inserted in list
    * @return Boolean result of action; true if successful, false otherwise
    */
    public boolean insertAfterCurrent(StudentClass newValue)
        {
         return iteratorList.insertDataAtNthPosition(currentIndex, newValue);
        }
    
    /*
    * Inserts new before value at iterator cursor
    * <p>
    * Note: Current value must remain the same after data set,
    * unless the list was empty prior to data set
    * @param newValue - StudentClass object to be inserted in list
    * @return Boolean result of action; true if successful, false otherwise
    */
    public boolean insertBeforeCurrent(StudentClass newValue)
        {
         boolean insertResult = iteratorList.insertDataAtNthPosition(currentIndex - 1, newValue);
         moveNext();
         return insertResult;
        }
    
    /*
    * Reports if iterator cursor is at beginning
    * If list is empty, cursor is not at beginning
    * @return Boolean result of action; true if at beginning, false otherwise
    */
    public boolean isAtBeginning()
        {
         return !( currentIndex != 1 || isEmpty() );
        }
    
    /*
    * Reports if iterator cursor is at end
    * If list is empty, cursor is not at end
    * @return Boolean result of action; true if at end, false otherwise
    */
    public boolean isAtEnd()
        {
         return !( currentIndex != iteratorList.getCurrentSize() || isEmpty() );
        }
    
    /*
    * Reports if list is empty
    * @return Boolean result of action; true if empty, false otherwise
    */
    public boolean isEmpty()
        {
         return iteratorList.isEmpty();
        }
    
    /*
    * If possible, moves iterator cursor one position to the right, or next
    * @return Boolean result of action; true if successful, false otherwise
    */
    public boolean moveNext()
        {
        if(!isEmpty() && !isAtEnd())
            {
             currentIndex++;
             return true;
            }
         return false;
        }
    
    /*
    * If possible, moves iterator cursor one position to the left, or previous
    * @return Boolean result of action; true if successful, false otherwise
    */
    public boolean movePrev()
        {
         if( !isEmpty() && !isAtBeginning() )
            {
             currentIndex--;
             return true;
            }
         return false;
        }
    
    /*
    * Removes and returns a data value from the iterator cursor position
    * <p>
    * Note: cursor must be located at succeeding element
    * unless last item removed
    * @return StudentClass object removed from list, or null if not found
    */
    public StudentClass removeAtCurrent()
        {
         return iteratorList.removeNthStudent( currentIndex );
        }
    
    /*
    * Replaces value at iterator cursor with new value
    * @param newValue - StudentClass object to be inserted in list
    * @return Boolean result of action; true if successful, false otherwise
    */
    public boolean replaceAtCurrent( StudentClass newValue )
        {
         return iteratorList.replaceDataAtNthPosition( currentIndex, newValue );
        }
    
    /*
    * Shows space-delimited list with cursor location indicated
    */
    public void runDiagnosticDisplay()
        {
         System.out.println( "Left end of Iterator:" );
         int loopCtr = 1;
         for(; loopCtr <= iteratorList.getCurrentSize(); loopCtr++)
            {
             displaySpaces(loopCtr);
             if( getAtCurrent().compareTo( iteratorList.getNthStudent(
                                                            loopCtr ) ) == 0 )
                {
                 System.out.println( "[" + iteratorList.getNthStudent(
                                                            loopCtr ) + "]" );
                }
             else 
                {
                 System.out.println( iteratorList.getNthStudent( loopCtr ) );
                }
            }
        }
    
    /*
    * Sets iterator cursor to beginning of list
    * @return Boolean result of action; true if successful, false otherwise
    */
    public boolean setToBeginning()
        {
         if( !isEmpty() )
            {
             currentIndex = 1;
             return true;
            }
         return false;
        }
    
    /*
    * Sets iterator cursor to the end of the list
    * @return Boolean result of action; true if successful, false otherwise
    */
    public boolean setToEnd()
        {
         if( !isEmpty() )
            {
             currentIndex = iteratorList.getCurrentSize();
             return true;
            }
         return false;
        }
}