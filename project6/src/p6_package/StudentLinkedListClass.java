
package p6_package;


public class StudentLinkedListClass {
    
    /*
    * For N not found in search
    */
    public static final int NOT_FOUND = -1;
    
    /*
    * Member Data
    */
    private StudentClass headRef;
    
    
    /*
    * Default constructor, initializes linked list
    */
    public StudentLinkedListClass()
        {
            headRef = null;
        }
    
    /*
    * Copy constructor, creates new nodes to eliminate aliasing
    * @param copied - StudentListClass object to be copied
    */
    public StudentLinkedListClass( StudentLinkedListClass copied )
        {
         this.headRef = copied.headRef;
        }
    
    /*
    * Sets data at end of list
    * <p>
    * Note: Uses appendDataAtEnd_Helper
    * @param student - StudentClass object to be appended to list
    */
    public void appendDataAtEnd( StudentClass student )
        {
            if( isEmpty() )
            {
             headRef = student;
             headRef.nextRef = null;
            }
            else
            {
             appendDataAtEnd_Helper( headRef, student );
            }
         
        }
    
    /*
    * Recursive helper method that appends data to end of list
    * @param wkgRef - StudentClass reference for linking nodes
    * @param student - StudentClass data to be added to the list
    * @return StudentClass link to calling method
    */
    private StudentClass appendDataAtEnd_Helper( StudentClass wkgRef,
                                            StudentClass student )
        {
         if( wkgRef.nextRef == null )
            {
             wkgRef.nextRef = student;
             wkgRef.nextRef.nextRef = null;
            }
         else
            {
             wkgRef.nextRef = appendDataAtEnd_Helper( wkgRef.nextRef, student );
            }
            return wkgRef;    
        }
    
    /*
    * Clears linked list of all valid values by setting linked list head to null,
    */
    public void clear()
        {
         this.headRef = null;
        }
    
    /*
    * Displays student list
    */
    public void displayList()
        {
         System.out.println( "Student Class List:" );
         StudentClass wkgRef = headRef;
         while( wkgRef != null )
            {
             System.out.println( wkgRef.name );
             wkgRef = wkgRef.nextRef;
            }
         System.out.println( "\n" );
        }
    
    /*
    * Gets number of student if found in list
    * @param student - StudentClass object for finding N
    * @return integer N of the StudentClass object, or NOT_FOUND if not in list
    */
    public int findStudentNumber( StudentClass student )
        {
         int studentIdx = 1;
         StudentClass tempStd = headRef;
         while( tempStd.nextRef != null )
            {
             if( student.compareTo( tempStd ) == 0 )
                {
                 return studentIdx;
                }
             tempStd = tempStd.nextRef;
             studentIdx++;
            }
         return studentIdx;
        }
    
    /*
    * Description: Gets current size of linked list
    * <p>
    * Note: Uses getCurrentSize_Helper
    * @return integer size of linked list
    */
    public int getCurrentSize()
        {
         if( isEmpty() )
            {
             return 0;
            }
         StudentClass tempStd = headRef;
         int temp = getCurrentSize_Helper( tempStd );
         return temp;
        }
    
    /*
    * Recursive helper method for finding list size
    * @param wkgRef - StudentClass reference for recursion management
    * @return integer size at each recursive level
    */
    private int getCurrentSize_Helper( StudentClass wkgRef )
        {
         if( wkgRef.nextRef != null )
            {
             return getCurrentSize_Helper( wkgRef.nextRef );
            }
         else
            {
             return findStudentNumber( wkgRef );
            }
        }
    
    /*
    * Acquires the Nth item in the list, starting with N = 1
    * @param N_value - integer N value for accessing student
    * @return StudentClass value at element or null if attempt
    * to acquire data out of bounds
    */
    public StudentClass getNthStudent( int N_value )
        {
         if( N_value > getCurrentSize() )
            {
             return null; 
            }
         int counter = 0;
         StudentClass wkgRef = headRef;
         while( counter < N_value - 1 )
            {
             wkgRef = wkgRef.nextRef;
             counter++;
            }
         return wkgRef;
        }
    
    /*
    * Sets data at beginning of list;moves all subsequent data up by one element
    * Note: No failure mode; data will be inserted at beginning
    * no matter what the size of the linked list is
    * @param student - StudentClass object to set at beginning
    */
    public void insertDataAtBeginning( StudentClass student )
        {
         StudentClass tempStudent = headRef;
         headRef = student;
         student.nextRef = tempStudent;
        }
    
    /*
    * Description: Links data into list at Nth position, where N starts at 1
    * <p>
    * Note: Allows item to be appended to end of list
    * @param NthPos - integer value to indicate insertion location
    * @param StudentClass object to be inserted at Nth position in list
    * @return Boolean indication of successful operation
    */
    public boolean insertDataAtNthPosition( int NthPos,
                                       StudentClass student )
        {
         if( NthPos == getCurrentSize() )
            {
             appendDataAtEnd( student );
             return true;
            }
         else if( NthPos == 1 )
            {
             insertDataAtBeginning( student );
             return true;
            }
        else if( NthPos < 1 || NthPos > getCurrentSize() + 1 )
            {
             return false;
            } 
         StudentClass lastStudent = getNthStudent( NthPos - 1 );
         StudentClass nextStudent = getNthStudent( NthPos );
         lastStudent.nextRef = student;
         student.nextRef = nextStudent;
         return true;
        }
    
    /*
    * Checks for empty list
    * @return Boolean result of test for empty
    */
    public boolean isEmpty()
        {
         return headRef == null;
        }
    
    /*
    * Description: Removes Nth item from linked list
    * @param numberN - integer number of element value to be removed, starts at N = 1
    * @return removed StudentClass value if successful, null if not
    */
    public StudentClass removeNthStudent( int numberN )
        {
         if( numberN < 1 || numberN > getCurrentSize() )
            {
             return null;      
            }
         StudentClass tempStudent = getNthStudent( numberN );
         if( numberN == 1 )
            {
             headRef = getNthStudent( numberN );
             return tempStudent;
            }
         else if( numberN == getCurrentSize() )
            {
             getNthStudent( numberN - 1 ).nextRef = null;
             return tempStudent;
            }
         else
            {
              StudentClass lastStudent = getNthStudent( numberN - 1 );
              lastStudent.nextRef = tempStudent.nextRef;
              return tempStudent;    
            }
        }
    
    /*
    * Description: Replaces item in linked list at specified Nth position, where N starts at 1
    * @param NthPos - integer value to indicate position of value to be replaced
    * @param student - StudentClass object to be inserted at Nth position
    * @return Boolean success if replaced, or failure if incorrect N was used
    */
    public boolean replaceDataAtNthPosition( int NthPos,
                                        StudentClass student )
        {
         if( NthPos < 1 || NthPos > getCurrentSize() )
            {
             return false;
            }
         StudentClass lastStudent = getNthStudent( NthPos - 1 );
         StudentClass nextStudent = getNthStudent( NthPos );
         lastStudent.nextRef = student;
         student.nextRef = nextStudent;
         return true;
        }
    }
