package p6_package;

public class StudentLinkedStackClass extends StudentLinkedListClass {
    
    /*
    * Constant for displaying spaces
    */
    private static final String SPACE = " ";
    
    /*
    * Default constructor
    */
    public StudentLinkedStackClass()
        {
         super();
        }
    
    /*
    * Copy constructor
    * @param copied - StudentStackClass object to be copied
    */
    public StudentLinkedStackClass( StudentLinkedStackClass copied )
        {
         super(copied);
        }
    
    /*
    * Clears stack using parent's operation
    */
    public void clearStack()
        {
         super.clear();
        }
    
    /*
    * Recursive method displays spaces for displayStack
    * @param numSpaces - integer value specifying number of spaces to display
    */
    private void displaySpaces( int numSpaces )
        {
         if( numSpaces > 0 )
            {
             displaySpaces( numSpaces - 1 );        
            }
         System.out.print( SPACE );
        }
    
    /*
    * Displays stack from bottom to top
    */
    public void displayStack()
        {
         System.out.println( "Bottom of Stack:" );
         int loopCtr;
        for( loopCtr = 1; loopCtr <= super.getCurrentSize(); loopCtr++ )
            {
              displaySpaces( loopCtr );
              System.out.println( super.getNthStudent( loopCtr ) );
            }
        displaySpaces( loopCtr +  1);
        System.out.print( "Top of Stack\n" );
        }
    /*
    * Reports stack empty using parent's operation
    * @return Boolean result of empty test
    */
    @Override
    public boolean isEmpty()
        {
         return super.isEmpty();
        }
    
    /*
    * Peeks at the top of the stack, no state change
    * @return StudentClass object viewed at the top of the stack
    */
    public StudentClass peekTop()
        {
         return super.getNthStudent( super.getCurrentSize() - 1 );
        }
    
    /*
    * Pops from stop of stack
    * @return StudentClass object popped from stack
    */
    public StudentClass pop()
        {
         return super.removeNthStudent( super.getCurrentSize() );
        }
    
    /*
    * Pushes onto top of stack
    * @param student - StudentClass object to be pushed onto stack
    */
    public void push( StudentClass student )
        {
         super.appendDataAtEnd( student );
        }
}