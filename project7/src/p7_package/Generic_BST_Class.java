
package p7_package;

public class Generic_BST_Class<GenericData extends Comparable<GenericData>> {
    
    private class Node {
        
        /*
        * generic data held by node
        */
        private GenericData nodeData;
        
        /*
        * left child reference
        */
        private Node leftChildRef;
        
        /*
        * right child reference
        */
        private Node rightChildRef;
        
        /* 
        * Initialization constructor for data
        * @param inData - GenericData quantity
        */
        public Node( GenericData inData )
            {
             nodeData = inData;
             leftChildRef = null;
             rightChildRef = null;
            }
    }
    
    /*
    * Traverse code - inorder
    */
    public static final int IN_TRAVERSE = 102;
    
    /*
    * Traverse code - postorder
    */
    public static final int POST_TRAVERSE = 103;
    
    /*
    * Traverse code - preorder
    */
    public static final int PRE_TRAVERSE = 101;
    
    /*
    * Root of BST
    */
    private Node rootNode;
    
    /*
    * Default class constructor, initializes BST
    */
    public Generic_BST_Class()
        {
         rootNode = new Node( null );
        }
    
    /*
    * Clears Tree
    */
    public void clearTree()
        {
         rootNode.nodeData = null;
         rootNode.leftChildRef = null;
         rootNode.rightChildRef = null;
        }
    
    /*
    * Provides inOrder traversal action using recursion
    * @param wkgRef - Node tree root reference at the current recursion level
    */
    private void displayInOrder( Node wkgRef )
        {
         if( wkgRef != null )
            {
             displayInOrder( wkgRef.leftChildRef );
             System.out.println( wkgRef.nodeData );
             displayInOrder( wkgRef.rightChildRef );
            }
        }
    
    /*
    * Provides postOrder traversal action using recursion
    * @param wkgRef - Node tree root reference at the current recursion level
    */
    private void displayPostOrder( Node wkgRef )
        {
        if( wkgRef != null )
            {
             displayPostOrder( wkgRef.leftChildRef );
             displayPostOrder( wkgRef.rightChildRef );
             System.out.println( wkgRef.nodeData );
            }
        }
    
    /*
    * Provides preOrder traversal action using recursion
    * @param wkgRef - Node tree root reference at the current recursion level
    */
    private void displayPreOrder( Node wkgRef )
        {
            if( wkgRef != null )
            {
             System.out.println( wkgRef.nodeData );
             displayPreOrder( wkgRef.leftChildRef );
             displayPreOrder( wkgRef.rightChildRef );
            }
        }
    
    /*
    * Provides user with three ways to display BST data
    * @param traverseCode - int code for selecting BST traversal method, accepts
    * PRE_TRAVERSE, IN_TRAVERSE, POST_TRAVERSE
    */
    public void displayTree( int traverseCode )
        {
        switch ( traverseCode ) 
            {
             case PRE_TRAVERSE:
                 displayPreOrder( rootNode );
                 break;
             case IN_TRAVERSE:
                 displayInOrder( rootNode );
                 break;
             default:
                 displayPostOrder( rootNode );
                 break;
            }
        }
    
    /*
    * Insert method for BST
    * Note: method adds first node if tree is empty;
    * otherwise calls insertHelper method
    * @param inData - GenericData data to be added to BST
    * @return Boolean result of operation
    */
    public boolean insert( GenericData inData )
        {
         if( isEmpty() )
            {
             rootNode.nodeData = inData;
             return true;
            }
             return insertHelper( rootNode, inData );
        }
    
    /*
    * Recursive insert helper method for BST insert action
    * Adds new node to left or right of current node; 
    * does not allow duplicate values to be inserted into tree
    * @param wkgRef - Node tree root reference at the current recursion level
    * @param inData - GenericData item to be added to BST
    * @return Boolean result of operation
    */
    private boolean insertHelper( Node wkgRef,
                             GenericData inData )
    {
     if( wkgRef.nodeData.compareTo(inData) < 0 )
        {
         if( wkgRef.rightChildRef != null )
            {
             return insertHelper( wkgRef.rightChildRef, inData );
            }
        else
            {
             wkgRef.rightChildRef = new Node( inData );
             return true;
            }
        }
     else if( wkgRef.nodeData.compareTo(inData) > 0 )
        {
         if( wkgRef.leftChildRef != null )
            {
             return insertHelper( wkgRef.leftChildRef, inData );
            }
        else
            {
             wkgRef.leftChildRef = new Node( inData );
             return true;
            }
        }
     else
        {
         return false; 
        }
    }
    
    /*
    * Test for empty tree
    * @return Boolean result for test
    */
    public boolean isEmpty()
        {
         return rootNode.nodeData == null;
        }
    
    /*
    * Searches tree from given node to minimum value node below it, 
    * stores data value found, unlinks the node, 
    * and returns it to the calling method Parameters:
    * @param parentNode - Node reference to current node
    * @param childnode - Node reference to child node to be tested
    * @return Node reference containing removed node
    */
    
    private Node removeFromMin( Node 
                                  parentNode, Node childNode )
        {
         if( childNode.leftChildRef != null )
            {
             return removeFromMin( childNode, childNode.leftChildRef );
            }
         
         parentNode.leftChildRef = childNode.rightChildRef;
         return childNode;
        }
    
    /*
    * Removes data node from tree using given key
    * <p>
    * Note: uses remove helper method
    * </p>
    * Note: uses search initially to get value,
    * if it is in tree; if value found, remove helper method is called,
    * otherwise returns null
    * @param inData - GenericData that includes the necessary key
    * @return GenericData result of remove action
    */
    public GenericData removeItem( GenericData inData )
        {
         GenericData searchResult = search( inData );
         if( searchResult != null )
            {
             removeItemHelper( rootNode, searchResult );
             return inData;
            }
         return null;
        }
    
    /*
    * Remove helper for BST remove action
    * <p>
    * Note: Recursive method returns updated local root to maintain tree linkage
    * <p>
    * Note: uses removeFromMin method
    * @param wkgRef - Node tree root reference at the current recursion level
    * @param outData - GenericData item that includes the necessary key
    * @return Node reference result of remove helper action
    */
    private Node removeItemHelper( Node wkgRef, GenericData outData )
        {
         if( wkgRef == null )
            {
             return wkgRef;
            }
        if( wkgRef.nodeData.compareTo( outData ) > 0 ) 
            {
             wkgRef.leftChildRef = removeItemHelper(
                                                wkgRef.leftChildRef, outData );
            }
        else if( wkgRef.nodeData.compareTo( outData ) < 0 )
            {
             wkgRef.rightChildRef = removeItemHelper(
                                                wkgRef.rightChildRef, outData );
            }
        else
            {
             if( wkgRef.leftChildRef == null )
                {
                wkgRef = wkgRef.rightChildRef;
                 return wkgRef;
                }
             else if( wkgRef.rightChildRef == null )
                {
                 wkgRef = wkgRef.leftChildRef;
                 return wkgRef;
                }
             else
                {
                 wkgRef.nodeData = (GenericData) removeFromMin( wkgRef, 
                                   wkgRef.rightChildRef.leftChildRef ).nodeData;
                }
            }
            return wkgRef;
        }
    
    /*
    * Searches for data in BST given GenericData with necessary key
    * @param searchData - GenericData item containing key
    * @return GenericData reference to found data
    */
    public GenericData search( GenericData searchData )
        {
         return (GenericData) searchHelper( rootNode, searchData );
        }
    
    /*
    * Helper method for recursive BST search action
    * @param wkgRef - Node tree root reference at the current recursion level
    * @param searchData - GenericData item containing key
    * @return GenericData item found
    */
    private GenericData searchHelper( Node wkgRef, GenericData searchData )
        {
         if( isEmpty() || wkgRef == null )
            {
             return null;
            }
         if ( wkgRef.nodeData.compareTo(searchData) == 0 )
            {
             return searchData;
            }
         if( wkgRef.nodeData.compareTo(searchData) > 0 )
            {
             return searchHelper( wkgRef.leftChildRef, searchData );
            }
         else
            {
             return searchHelper( wkgRef.rightChildRef, searchData );
            }
        }
}