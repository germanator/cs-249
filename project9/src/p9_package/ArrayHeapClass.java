
package p9_package;

public class ArrayHeapClass {
    
    /*
    * Initial array capacity
    */
    public final int DEFAULT_ARRAY_CAPACITY = 10;
    
    /*
    * Constant for not found value
    */
    public final int NOT_FOUND = 99999;
    
    /*
    * Array for heap
    */
    private int[] heapArray;
    
    /*
    * Management data for array
    */
    private int arraySize;
    
    /*
    * Management data for array
    */
    private int arrayCapacity;
    
    /*
    * Display flag can be set to observe bubble up and trickle down operations
    */
    private boolean displayFlag;

    /*
    * Default constructor sets up array management conditions
    * and default display flag setting
    */
    public ArrayHeapClass()
        {
         displayFlag = true;
         arrayCapacity = DEFAULT_ARRAY_CAPACITY;
         heapArray = new int[ arrayCapacity ];
         arraySize = 0;
        }

    /*
    * Copy constructor copies array and array management
    * conditions and default display flag setting
    * @param copied - ArrayHeapClass object to be copied
    */
    public ArrayHeapClass( ArrayHeapClass copied )
        {
         arrayCapacity = copied.arrayCapacity;
         displayFlag = copied.displayFlag;
         arraySize = copied.arraySize;
         heapArray = new int[ arrayCapacity ];
         int copyCtr = 0;
         for( ; copyCtr < arraySize; copyCtr++ )
            {
             heapArray[ copyCtr ] = copied.heapArray[ copyCtr ];
            }
        }

    /*
    * Accepts integer item and adds it to heap
    * <p>
    * Note: uses bubbleUpArrayHeap to resolve unbalanced heap after data addition
    * <p>
    * Note: Always checks for resize before adding data
    * @param newItem - integer item to be added
    */
    public void addItem( int newItem )
        {
         checkForResize();
         heapArray[ arraySize ] = newItem;
         if( displayFlag )
            {
             System.out.println( "Adding new value: " + newItem );
            }
         bubbleUpArrayHeap( arraySize );
         arraySize++;
        }

    /*
    * Recursive operation to reset data in the correct
    * order for the max heap after new data addition
    * @param currentIndex - index of current item being assessed, and moved up as needed
    */
    private void bubbleUpArrayHeap( int currentIndex )
        {
         if( currentIndex > 0 && heapArray[ currentIndex ] >
                                        heapArray[ ( currentIndex - 1 )  / 2 ] )
            {
             if( displayFlag )
                {
                 System.out.println("   - Bubble up:");
                 System.out.println("      - Swapping parent: " + 
                                           heapArray[ (currentIndex  - 1) / 2] +
                                   " with child: " + heapArray[ currentIndex ]);
                 
                }
             int tempValue = heapArray[ currentIndex ];
             heapArray[ currentIndex ] = heapArray[ ( currentIndex - 1 ) / 2 ];
             heapArray[ ( currentIndex - 1 ) / 2 ] = tempValue;
             bubbleUpArrayHeap( ( currentIndex - 1 ) / 2 );           
            }
        }

    /*
    * Automatic resize operation used prior to any new data addition in the heap
    * <p>
    * Note: Tests for full heap array, and resizes
    * to twice the current capacity as required
    */
    private void checkForResize()
        {
         if( arraySize >= arrayCapacity )
            {
             arrayCapacity *= 2;
             int[] newHeap = new int[ arrayCapacity ];
             int copyIndex = 0;
             for( ; copyIndex < arraySize; copyIndex++ )
                {
                 newHeap[ copyIndex ] = heapArray[ copyIndex ];    
                }
             heapArray = newHeap;
            }
        }
    
    /*
    * Tests for empty heap
    * @return boolean result of test
    */
    public boolean isEmpty()
        {
         return arraySize == 0;
        }
    
    /*
    * Removes integer item from top of max heap
    * <p>
    * Note: Uses trickleDownArrayHeap to resolve unbalanced heap after data removal
    * @return integer item removed
    */
    public int removeItem()
        {
            if( !isEmpty() )
            {
             int itemRemoved = heapArray[ 0 ];
             heapArray[ 0 ] = heapArray[ arraySize - 1 ];
             arraySize--;
             System.out.println( "Removing value: " + itemRemoved );
             trickleDownArrayHeap( 0 );
             return itemRemoved;
            }
         return NOT_FOUND;
        }
    
    /*
    * Utility method to set the display flag for displaying internal operations
    * of the heap bubble and trickle operations
    * @param setState - flag used to set the state to display, or not
    */
    public void setDisplayFlag( boolean setState )
        {
         displayFlag = setState;
        }
    
    /*
    * Dumps array to screen as is, no filtering or management
    */
    public void showArray()
        {
         int displayCtr = 0;
         for( ; displayCtr < arraySize; displayCtr++ )
            {
             System.out.print( heapArray[ displayCtr ] + " " );
            }
        }
    
    /*
    * Recursive operation to reset data in the correct order
    * for the max heap after data removal
    * @param currentIndex - index of current item being assessed, and moved down as required
    */
    private void trickleDownArrayHeap( int currentIndex )
        {
         String childSide = "left";
         int childIndex = ( currentIndex + 1 ) * 2 - 1;
         if( childIndex < arraySize )
            {
             if( childIndex + 1 < arraySize && heapArray[ childIndex ] <
                                                    heapArray[ childIndex + 1 ])
                {
                 childSide = "right";
                 childIndex++;
                }
                
             if( heapArray[ childIndex ] > heapArray[ currentIndex ] )
                {
                 int tempValue = heapArray[ currentIndex ];
                 heapArray[ currentIndex ] = heapArray[ childIndex ];
                 heapArray[ childIndex ] = tempValue;
                 if( displayFlag )
                    {
                     System.out.println("   - Trickle Down:" ); 
                     System.out.println("      - Swapping Parent: " + 
                                heapArray[ childIndex ] + " with " + childSide +
                                       " child: " + heapArray[ currentIndex ] ); 
                    }
                }
             trickleDownArrayHeap( childIndex );
            }
        }   
}
